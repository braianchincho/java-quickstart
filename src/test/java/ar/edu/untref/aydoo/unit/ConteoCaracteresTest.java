package ar.edu.untref.aydoo.unit;

import ar.edu.untref.aydoo.ConteoCaracteres;
import org.junit.Assert;
import org.junit.Test;

public class ConteoCaracteresTest {

    //zombie = zero, one, many, bonders, interface, exception;

    //zero
    @Test
    public void cadenaVaciaRetorna0Caracteres(){
        ConteoCaracteres conteo = new ConteoCaracteres();
        int result = conteo.contarCaracteres("");
        Assert.assertEquals(0, result);
    }
}
