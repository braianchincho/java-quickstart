package ar.edu.untref.aydoo.unit;
import ar.edu.untref.aydoo.Calculadora;
import org.junit.Test;
import org.junit.Assert;

public class CalculadoraTest {

    @Test
    public void testSuma1Mas2Es3(){
        Calculadora calculadora = new Calculadora();
        Integer resultado =calculadora.sumar(1,2);
        Assert.assertEquals(new Integer(3), resultado);
    }
    @Test
    public void testSuma2Mas2Es4(){
        Calculadora calculadora = new Calculadora();
        Integer resultado =calculadora.sumar(2,2);
        Assert.assertEquals(new Integer(4), resultado);
    }
}
