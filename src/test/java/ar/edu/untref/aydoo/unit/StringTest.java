package ar.edu.untref.aydoo.unit;

import org.junit.Test;
import org.junit.Assert;

public class StringTest {


    @Test
    public void testIsEmptyDevuelveFalseCuandoNoHayCadenaVacia(){
        String str = "Hola mundo";
        boolean result = str.isEmpty();
        Assert.assertEquals(false, result);
    }
    @Test
    public void testIsEmptyDevuelveFalseCuandoLaCadenaEsVacia(){
        String str = "";
        boolean result = str.isEmpty();
        Assert.assertEquals(true, result);
    }
    @Test
    public void testIndexOfCuandoCoincideElIndiceConElCaracterIngresado(){
        String str = "Hola mundo";
        int result = str.indexOf('o');
        Assert.assertEquals(1, result);
    }

    @Test
    public void testUpperCaseCuandoPasaAMayusculaUnString(){
        String str = "Hola mundo";
        String result = str.toUpperCase();
        Assert.assertEquals("HOLA MUNDO", result);
    }

    @Test
    public void testStartWithDevuelveTrueCuandoCoincide(){
        String str = "Hola mundo";
        boolean result = str.startsWith("H");
        Assert.assertTrue(result);
    }

}
